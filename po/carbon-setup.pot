# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the carbon-setup package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: carbon-setup\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-07 02:52-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. TRANSLATORS: The display name of the demo user. Use whatever word
#. demonstrates that most clearly; doesn't have to be "demo"
#: src/main.vala:106
msgid "Demo"
msgstr ""

#: src/pages/disclaimer/disclaimer.vala:35
msgid "Disclaimer"
msgstr ""

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/disclaimer/disclaimer.vala:37
#, c-format
msgid ""
"This is an early development build of %s. Please be aware that breaking "
"changes may still occur. You may need to manually migrate data after such a "
"change. We provide no stability or security guarantees. Back up all "
"important data. Do not use in a production environment."
msgstr ""

#: src/pages/done/done.vala:19
msgid "Almost Done…"
msgstr ""

#: src/pages/done/done.vala:20
msgid "You're all set up!"
msgstr ""

#: src/pages/done/done.vala:22
msgid "Your computer needs to restart to finish the setup"
msgstr ""

#: src/pages/done/done.vala:23
msgid "Your computer is now ready to be used"
msgstr ""

#: src/pages/done/done.vala:25
msgid "_Reboot"
msgstr ""

#. TRANSLATORS: This button completes setup and takes the user to
#. their new desktop
#: src/pages/done/done.vala:28
msgid "_Done"
msgstr ""

#: src/pages/done/done.vala:42
msgid "Reboot"
msgstr ""

#: src/pages/drive-select/drive-select.vala:31
msgid "Select a Drive"
msgstr ""

#: src/pages/drive-select/drive-select.vala:32
msgid ""
"All of the data will be erased from the drive you select. Make sure you have "
"any important data backed up."
msgstr ""

#. TRANSLATORS: This line tells the user no drives were detected. The
#. "found" language here doesn't indicate a search
#: src/pages/drive-select/drive-select.vala:35
msgid "No Drives Found"
msgstr ""

#: src/pages/drive-select/drive-select.vala:36
msgid "Are you sure?"
msgstr ""

#. TRANSLATORS: Confirm that the user is ok with the drive being wiped
#. and start the installation of the OS
#: src/pages/drive-select/drive-select.vala:39
msgid "Co_ntinue"
msgstr ""

#: src/pages/drive-select/drive-select.vala:40
msgid "_Cancel"
msgstr ""

#. TRANSLATORS: Format is the name of a disk drive, for example:
#. "WD Blue" or "Samsung 840 EVO"
#: src/pages/drive-select/drive-select.vala:145
#, c-format
msgid "All of the apps and data on this <b>%s</b> will be erased!"
msgstr ""

#: src/pages/hostname/hostname.vala:17
msgid "Name this Device"
msgstr ""

#: src/pages/hostname/hostname.vala:18
msgid ""
"This is used to identify this device when communicating with other devices "
"nearby and on the network."
msgstr ""

#. TRANSLATORS: This describes what the window is/does. Not a command.
#. The %s will be replaced with the name of the OS
#: src/pages/installer-welcome/installer-welcome.vala:13
#, c-format
msgid "Install %s"
msgstr ""

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/installer-welcome/installer-welcome.vala:15
#, c-format
msgid "%s will be installed onto this device"
msgstr ""

#. TRANSLATORS: Install step: creating filesystem layout
#: src/pages/install-progress/install-progress.vala:31
msgid "Initializing…"
msgstr ""

#. TRANSLATORS: Install step: Copying the OS
#: src/pages/install-progress/install-progress.vala:36
msgid "Copying Files…"
msgstr ""

#. TRANSLATORS: Install step: copying w/ percent complete
#: src/pages/install-progress/install-progress.vala:45
#, c-format
msgid "Copying Files… (%d%%)"
msgstr ""

#. TRANSLATORS: Install step: merging OS and kernel into final image
#: src/pages/install-progress/install-progress.vala:51
msgid "Merging…"
msgstr ""

#. TRANSLATORS: Install step: Turning the image into a bootable OS
#: src/pages/install-progress/install-progress.vala:56
msgid "Deploying…"
msgstr ""

#. TRANSLATORS: Install step: deleting temporary files
#: src/pages/install-progress/install-progress.vala:61
msgid "Cleaning up…"
msgstr ""

#. TRANSLATORS: Install step: Wiping disk & creating partitions
#: src/pages/install-progress/install-progress.vala:89
msgid "Formatting…"
msgstr ""

#. TRANSLATORS: Install step: Installing the bootloader
#: src/pages/install-progress/install-progress.vala:139
msgid "Installing bootloader…"
msgstr ""

#. TRANSLATORS: Install step: Shutting down disk we just installed to
#: src/pages/install-progress/install-progress.vala:160
msgid "Finalizing…"
msgstr ""

#: src/pages/kbd-layout/kbd-layout-row.ui:42
msgid "Preview"
msgstr ""

#: src/pages/kbd-layout/kbd-layout.vala:21
msgid "Keyboard"
msgstr ""

#: src/pages/kbd-layout/kbd-layout.vala:22
msgid "Test your settings here"
msgstr ""

#. TRANSLATORS: Found here means that the user searched for a
#. keyboard layout that isn't in the list.
#: src/pages/kbd-layout/kbd-picker.vala:229
msgid "No Keyboard Layouts Found"
msgstr ""

#: src/pages/kbd-layout/kbd-picker.vala:230
#: src/pages/language/lang-picker.vala:185
msgid "Search"
msgstr ""

#. TRANSLATORS: Found here means that the user searched for a
#. language that isn't in the list.
#: src/pages/language/lang-picker.vala:184
msgid "No Languages Found"
msgstr ""

#: src/pages/language/lang-picker.vala:186
msgid "More…"
msgstr ""

#. TRANSLATORS: This should be a warm, welcoming message. Someething
#. you'd say to greet someone at the door
#: src/pages/language/language.vala:28
msgid "Welcome!"
msgstr ""

#: src/pages/network/network.vala:46 src/pages/network/network.vala:49
msgid "Connect to Wi-Fi"
msgstr ""

#: src/pages/network/network.vala:47
msgid "Hidden Network…"
msgstr ""

#: src/pages/network/network.vala:48
msgid "You're Already Connected"
msgstr ""

#: src/pages/network/network.vala:50
msgid "No Network Devices"
msgstr ""

#: src/pages/network/network.vala:51
msgid ""
"No available network devices were found. You can continue setting up your "
"device without an internet connection."
msgstr ""

#: src/pages/network/network.vala:52
msgid "Plug in your Ethernet"
msgstr ""

#: src/pages/network/network.vala:53
msgid ""
"For a better experience, connect your device to the network via Ethernet. "
"You can continue setting up your device without an internet connection."
msgstr ""

#. TRANSLATORS: This is shown on the wifi selection page if the
#. device is already connected to ethernet
#: src/pages/network/network.vala:100
msgid "You're already connected via Ethernet."
msgstr ""

#. TRANSLATORS: This is shown on the wifi selection page if the
#. device is already connected to a mobile carrier network
#: src/pages/network/network.vala:107
msgid ""
"You're already connected to a mobile network. Connect to Wi-Fi for a better "
"experience."
msgstr ""

#. TRANSLATORS: This is shown on the wifi selection page if the
#. device has an ethernet port but is not connected to it
#: src/pages/network/network.vala:119
msgid "You can also connect via Ethernet."
msgstr ""

#: src/pages/network/network.vala:159
#: src/pages/try-or-install/try-or-install.vala:52
msgid "Feature isn't implemented"
msgstr ""

#: src/pages/network/network.vala:160
msgid "Setup cannot connect to hidden networks yet"
msgstr ""

#: src/pages/password/password.vala:19
msgid "Set a Password"
msgstr ""

#: src/pages/password/password.vala:20
msgid "Password"
msgstr ""

#. TRANSLATORS: Tells user to enter their password again to confirm it
#: src/pages/password/password.vala:22
msgctxt "Password"
msgid "Confirm"
msgstr ""

#: src/pages/theme/theme.vala:22
msgid "Appearance"
msgstr ""

#: src/pages/theme/theme.vala:23
msgid "Default"
msgstr ""

#: src/pages/theme/theme.vala:24
msgid "Dark"
msgstr ""

#: src/pages/timezone/timezone.vala:61
msgid "Timezone"
msgstr ""

#: src/pages/timezone/timezone.vala:62
msgid "Search for your city to set the clock"
msgstr ""

#: src/pages/timezone/timezone.vala:63
msgid "Is this the correct time?"
msgstr ""

#: src/pages/timezone/timezone.vala:64
msgid "No"
msgstr ""

#: src/pages/timezone/timezone.vala:65
msgid "Yes"
msgstr ""

#. The %Z format sometimes spits out ugly offsets. Let's
#. make them nicer
#. TRANSLATORS: UTC here means Coordinated Universal Time.
#. the %:z will be replaced by the offset (i.e. UTC+06:00)
#: src/pages/timezone/timezone.vala:212
msgid "UTC%:z"
msgstr ""

#: src/pages/try-or-install/try-or-install.vala:21
msgid "Try or Install?"
msgstr ""

#. TRANSLATORS: "Try" here means "Try carbonOS and see if you like it"
#. It's a demo or trial of the system
#: src/pages/try-or-install/try-or-install.vala:25
msgid "_Try"
msgstr ""

#: src/pages/try-or-install/try-or-install.vala:26
msgid ""
"Changes will not be saved. Your existing data will not be modified. "
"Performance and features may be limited"
msgstr ""

#: src/pages/try-or-install/try-or-install.vala:28
msgid "_Install"
msgstr ""

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/try-or-install/try-or-install.vala:30
#, c-format
msgid "Wipe the disk and install a fresh copy of %s"
msgstr ""

#: src/pages/try-or-install/try-or-install.vala:32
msgid "_Repair"
msgstr ""

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/try-or-install/try-or-install.vala:34
#, c-format
msgid "Repair an existing installation of %s"
msgstr ""

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/try-or-install/try-or-install.vala:54
#, c-format
msgid "Setup cannot repair %s installations yet"
msgstr ""

#: src/pages/user/user.vala:26
msgid "Create an Account"
msgstr ""

#: src/pages/user/user.vala:27
msgid "Full Name"
msgstr ""

#: src/pages/user/user.vala:28
msgid "Username"
msgstr ""

#: src/user.vala:91
msgid "Username is too long"
msgstr ""

#: src/user.vala:94
msgid ""
"Username must only contain letters, digits, and the following characters: - _"
msgstr ""

#: src/user.vala:97
msgid "Username must start with a letter"
msgstr ""

#: src/user.vala:100
msgid "This will be used to name your home folder and can't be changed"
msgstr ""

#: src/window.vala:32
msgid "_Skip"
msgstr ""

#: src/window.vala:33
msgid "_Next"
msgstr ""

#: src/window.vala:34
msgid "_Back"
msgstr ""
