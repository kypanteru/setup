[GtkTemplate(ui="/sh/carbon/setup/ui/installer-welcome.ui")]
class Setup.InstallerWelcomePage : Adw.Bin, Setup.Page {
    public bool ready { get { return true; } }

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label subtitle;
    [GtkChild] unowned Gtk.Image logo;

    void populate_text() {
        var os_name = Environment.get_os_info(OsInfoKey.NAME);
        // TRANSLATORS: This describes what the window is/does. Not a command.
        // The %s will be replaced with the name of the OS
        title.label = _("Install %s").printf(os_name);
        // TRANSLATORS: The %s will be replaced with the name of the OS
        subtitle.label = _("%s will be installed onto this device").printf(os_name);
    }

    void enter() {
        var style_mgr = Adw.StyleManager.get_default();
        style_mgr.notify["dark"].connect(() => update_style(style_mgr));
        update_style(style_mgr);
    }

    private void update_style(Adw.StyleManager mgr) {
        if (mgr.dark)
            logo.icon_name = "carbon-logo-dark";
        else
            logo.icon_name = "carbon-logo";
    }
}
