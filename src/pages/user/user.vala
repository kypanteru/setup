[GtkTemplate(ui="/sh/carbon/setup/ui/user.ui")]
class Setup.UserPage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }

    private Driver driver;
    private string? previous_username_suggestion = null;
    private uint timeout_update_avatar = 0;

    [GtkChild] unowned Adw.Avatar avatar;
    [GtkChild] unowned Gtk.Entry realname;
    [GtkChild] unowned Gtk.Entry username;
    [GtkChild] unowned Gtk.Label tip;

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label realname_lbl;
    [GtkChild] unowned Gtk.Label username_lbl;

    // TODO: Use a ListBox and Adw.EntryRow

    public UserPage(Driver driver) {
        this.driver = driver;
    }

    void populate_text() {
        title.label = _("Create an Account");
        realname_lbl.label = _("Full Name");
        username_lbl.label = _("Username");

        string tip_text;
        validate_username("", out tip_text);
        tip.label = tip_text;
    }

    [GtkCallback]
    void on_realname_changed() {
        var new_username_suggestion = suggest_username(realname.text);
        if (username.text == "" || username.text == previous_username_suggestion) {
            username.text = new_username_suggestion;
        }
        previous_username_suggestion = new_username_suggestion;

        ratelimit_update_avatar();
        invalidate_ready();
    }

    private bool validate() {
        var realname_valid = validate_realname(realname.text);
        if (!realname_valid)
            realname.add_css_class("error");
        else
            realname.remove_css_class("error");
        string tip_label;
        var username_valid = validate_username(username.text, out tip_label);
        tip.label = tip_label;
        if (!username_valid)
            username.add_css_class("error");
        else
            username.remove_css_class("error");
        return realname_valid && username_valid;
    }

    private void ratelimit_update_avatar() {
        if (timeout_update_avatar != 0)
            Source.remove(timeout_update_avatar);
        timeout_update_avatar = Timeout.add(600, () => {
            avatar.text = realname.text;
            timeout_update_avatar = 0;
            return Source.REMOVE;
        });
    }

    [GtkCallback]
    private void invalidate_ready() {
        _ready = validate();
        this.notify_property("ready");
    }

    [GtkCallback]
    private void maybe_next() {
        if (validate()) this.next();
    }

    bool apply() {
        var new_user = new NewUser();
        new_user.user_name = username.text;
        new_user.real_name = realname.text;
        new_user.language = driver.language;
        driver.new_user = new_user;
        return true;
    }
}
