using GDesktop;

[GtkTemplate(ui="/sh/carbon/setup/ui/theme.ui")]
class Setup.ThemePage : Adw.Bin, Setup.Page {
    public bool ready { get { return true; } }

    private Settings settings;

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.ToggleButton default_toggle;
    [GtkChild] unowned Gtk.ToggleButton dark_toggle;
    [GtkChild] unowned Gtk.Label default_label;
    [GtkChild] unowned Gtk.Label dark_label;

    construct {
        settings = new Settings("org.gnome.desktop.interface");
        settings.changed["color-scheme"].connect(load_selection);
        load_selection();
    }

    void populate_text() {
        title.label = _("Appearance");
        default_label.label = _("Default");
        dark_label.label = _("Dark");
    }

    private void load_selection() {
        var scheme = settings.get_enum("color-scheme");
        default_toggle.active = (scheme == ColorScheme.DEFAULT);
        dark_toggle.active = (scheme == ColorScheme.PREFER_DARK);
    }

    private async void screen_transition() {
        // HACK: Here we pretend we're org.gnome.Settings. We get away with this
        // since gnome-control-center will never run in the initial-setup session.
        // This works around a check gnome-shell enforces before allowing the
        // screen transition animation.
        var name = Bus.own_name(BusType.SESSION, "org.gnome.Settings",
                                BusNameOwnerFlags.ALLOW_REPLACEMENT |
                                BusNameOwnerFlags.DO_NOT_QUEUE,
                                null, () => screen_transition.callback(),
                                () => screen_transition.callback());
        yield;

        try {
            var conn = Bus.get_sync(BusType.SESSION);
            conn.call_sync("org.gnome.Shell", "/org/gnome/Shell", "org.gnome.Shell",
                "ScreenTransition", null, null, DBusCallFlags.NONE, -1);
        } catch (Error e) {
            critical("Failed to call ScreenTransition: %s", e.message);
        }

        Bus.unown_name(name);
    }

    private void set_color_scheme(ColorScheme new_value) {
        if (settings.get_enum("color-scheme") == new_value)
            return;

        screen_transition.begin();
        settings.set_enum("color-scheme", new_value);
    }

    [GtkCallback]
    void update_theme() {
        if (dark_toggle.active)
            set_color_scheme(ColorScheme.PREFER_DARK);
        else if (default_toggle.active)
            set_color_scheme(ColorScheme.DEFAULT);
    }
}

