[GtkTemplate(ui="/sh/carbon/setup/ui/theme-preview.ui")]
public class Setup.ThemePreview : Gtk.Widget {
    public bool is_dark { get; construct; default = false; }
    private Gdk.Pixbuf background_image;

    [GtkChild] unowned Gtk.DrawingArea background;
    [GtkChild] unowned Adw.Bin dark_window;
    [GtkChild] unowned Adw.Bin light_dark_window;

    static construct {
        set_css_name("background-preview");

        var provider = new Gtk.CssProvider();
        provider.load_from_resource("/sh/carbon/setup/theme-preview.css");
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );
    }

    construct {
        var filename = is_dark ?
                       "/usr/share/backgrounds/gnome/adwaita-d.webp" :
                       "/usr/share/backgrounds/gnome/adwaita-l.webp";
        try {
            background_image = new Gdk.Pixbuf.from_file(filename);
        } catch (Error e) {
            // This is fatal so that the bug is obvious, AND so we don't
            // have to write code to handle the scenario where those images
            // don't exist (this code should always work!)
            error("Failed to load bg image!");
        }
        background.set_draw_func(draw_bg);

        light_dark_window.add_css_class(is_dark ? "dark" : "light");
    }

    public override Gtk.SizeRequestMode get_request_mode() {
        return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH;
    }

    public override void measure(Gtk.Orientation orientation, int for_size,
                                 out int min, out int natural,
                                 out int min_baseline, out int nat_baseline) {
        min_baseline = -1;
        nat_baseline = -1;

        if (orientation == Gtk.Orientation.HORIZONTAL)
            natural = get_primary_monitor_geometry().width;
        else if (for_size < 0)
            natural = 0;
        else
            natural = (int) Math.floor(for_size * 0.75);

        if (orientation == Gtk.Orientation.VERTICAL)
            min = natural;
        else
            min = 0;

        for (var child = this.get_first_child();
             child != null;
             child = child.get_next_sibling()) {

            int child_min, child_nat;
            child.measure(orientation, for_size, out child_min, out child_nat,
                          null, null);

            min = int.max(child_min, min);
            natural = int.max(child_nat, natural);
        }
    }


    public override void size_allocate(int width, int height, int baseline) {
        var window_width = (int) Math.ceil(width * 0.5);
        var window_height = (int) Math.ceil(height * 0.5);
        var margin_x = (int) Math.floor(width * 0.15);
        var margin_y = (int) Math.floor(height * 0.15);
        var opposite_margin_x = width - window_width - margin_x;
        var opposite_margin_y = height - window_height - margin_y;
        var is_rtl = (this.get_direction() == Gtk.TextDirection.RTL);

        var front_transform = Graphene.Point() {
            x = is_rtl ? opposite_margin_x : margin_x,
            y = opposite_margin_y
        };
        var back_transform = ((Gsk.Transform) null).translate(Graphene.Point() {
            x = is_rtl ? margin_x : opposite_margin_x,
            y = margin_y
        });

        background.allocate(width, height, baseline, null);
        dark_window.allocate(window_width, window_height, baseline,
            back_transform);
        light_dark_window.allocate(window_width, window_height, baseline,
            ((Gsk.Transform) null).translate(front_transform));
    }

    private Gdk.Rectangle get_primary_monitor_geometry() {
        var monitors = Gdk.Display.get_default().get_monitors();
        if (monitors.get_n_items() == 0)
            return {0, 0, 1920, 1080};
        var primary = monitors.get_item(0) as Gdk.Monitor;
        return primary.geometry;
    }

    private void draw_bg(Gtk.DrawingArea drawing_area, Cairo.Context cr,
                         int width, int height) {
        var dest = new Gdk.Pixbuf(background_image.colorspace,
                                  background_image.has_alpha,
                                  background_image.bits_per_sample,
                                  width, height);
        scale_pixbuf(dest, background_image);
        Gdk.cairo_set_source_pixbuf(cr, dest, 0, 0);
        cr.paint();
    }


    private void scale_pixbuf(Gdk.Pixbuf dest, Gdk.Pixbuf src) {
        var scl_w = (double) dest.width / src.width,
            scl_h = (double) dest.height / src.height;
        var factor = scl_w > scl_h ? scl_w : scl_h;

        var final_width = (int)(factor * src.width),
            final_height = (int)(factor * src.height);
        var off_x = (dest.width - final_width) / 2,
            off_y = (dest.height - final_height) / 2;

        src.scale(dest, 0, 0, dest.width, dest.height, off_x, off_y, factor,
            factor, Gdk.InterpType.BILINEAR);
    }
}
