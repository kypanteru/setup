namespace CC {
    // For generating search keys from localized text
    [CCode(cname="cc_util_normalize_casefold_and_unaccent")]
    public extern string normalize_casefold_and_unaccent(string input);
}

namespace Setup.Lang {
    // Used when translating "Welcome" to different languages
    [CCode(cname="LC_MESSAGES_MASK", cheader_filename="locale.h")]
    public extern const int LC_MESSAGES_MASK;
    [CCode(cname="newlocale", cheader_filename="locale.h")]
    public extern void* newlocale(int mask, string id, void* base_locale);
    [CCode(cname="uselocale", cheader_filename="locale.h")]
    public extern void* uselocale(void* newlocale);
    [CCode(cname="freelocale", cheader_filename="locale.h")]
    public extern void freelocale(void* locale);

    // Tell localed about the selected locales
    [DBus(name="org.freedesktop.locale1")]
    private interface Localed : DBusProxy {
        public async abstract void set_locale(string[] locale, bool i)
            throws Error;
    }
    public async void set_localed_locale(string locale) {
        if (Driver.mock) {
            print("set_localed_locale: %s\n", locale);
            return;
        }

        try {
            var proxy = yield Bus.get_proxy<Localed>(BusType.SYSTEM,
                "org.freedesktop.locale1", "/org/freedesktop/locale1");
            yield proxy.set_locale({ locale }, true);
        } catch (Error e) {
            critical("Failed to set logind locale: %s", e.message);
        }
    }

    // Languages that show up in welcome spinner and as default list
    public const string[] INITIAL_LOCALES = {
        "en_US.UTF-8",
        //"de_DE.UTF-8",
        //"fr_FR.UTF-8",
        "es_ES.UTF-8",
        //"zh_CN.UTF-8",
        //"ja_JP.UTF-8",
        "ru_RU.UTF-8",
        //"ar_EG.UTF-8"
    };
}
