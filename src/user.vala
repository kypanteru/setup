namespace Setup {
    class NewUser {
        public string user_name;
        public string real_name;
        public string? password;
        public string? language;
        // TODO: Profile picture

        private Act.User? act_user = null;

        public string homedir {
            owned get {
                if (act_user == null)
                    return @"/var/home/$user_name";
                else
                    return act_user.home_directory;
            }
        }

        public int uid {
            get {
                if (act_user == null)
                    error("Tried to get UID user before creating it");
                return act_user.uid;
            }
        }

        public int gid {
            get {
                return (int) Posix.getpwuid(uid).pw_gid;
            }
        }

        public NewUser() {
            // Ask the kernel to protect the password field
            // TODO: is this right?
            Posix.mlock(&password, sizeof(string));
        }

        public async void create() {
            if (Driver.mock) {
                print("new_user: create: name='%s' real='%s' password='%s'\n",
                    user_name, real_name, password);
                return;
            }

            var mgr = Act.UserManager.get_default();
            try {
                act_user = yield mgr.create_user_async(this.user_name,
                    this.real_name, Act.UserAccountType.ADMINISTRATOR, null);
            } catch (Error e) {
                error("Failed to create user: %s", e.message);
            }
            if (this.password != null)
                act_user.set_password(this.password, "" /* no hint */);
            else
                act_user.set_password_mode(Act.UserPasswordMode.NONE);
            act_user.set_language(this.language ?? "" /* empty = use system */);
        }
    }

    // Manipulating usernames

    [CCode(cheader_filename="utmp.h", cname="UT_NAMESIZE")]
    private extern const int MAX_NAMESIZE;

    // Real name must not be empty and must not start with a space
    private bool validate_realname(string name) {
        return name.length > 0 && !name[0].isspace();
    }

    // Valid chars: a-z, A-Z, 0-9, ., -, _
    private bool is_valid_username_char(char c) {
        return c.isalpha() || c.isdigit() || c == '-' || c == '_';
    }

    // Username must:
    // 1. Must not be empty
    // 2. Be at most UT_NAMESIZE characters long
    // 3. Must satisfy is_valid_username_char
    // 4. Must begin with a letter
    private bool validate_username(string text, out string message) {
        var empty = text.length == 0;
        var too_long = text.length >= MAX_NAMESIZE;

        var valid_chars = true, starts_with_letter = true;
        for (int i = 0; i < text.length; i++) {
            char c = text[i];
            if (i == 0) starts_with_letter = c.isalpha();
            valid_chars &= is_valid_username_char(c);
        }

        if (too_long) {
            message = _("Username is too long");
            return false;
        } else if (!valid_chars) {
            message = _("Username must only contain letters, digits, and the following characters: - _");
            return false;
        } else if (!starts_with_letter) {
            message = _("Username must start with a letter");
            return false;
        } else {
            message = _("This will be used to name your home folder and can't be changed");
            return !empty;
        }
    }

    // Generate a suitable username from a full name
    private string suggest_username(string name) {
        if (!validate_realname(name)) return "";

        // First, split at spaces and come up with a raw string to sanitize
        var split = name.chomp().down().split(" ");
        var to_sanitize = split[0];
        string? last = null;
        if (split.length == 2) // Probably first + last
            last = split[1];
        else if (split.length == 3) // Probably first + middle name/initial + last
            last = split[2];
        if (last != null && last.length > 0) {
            if ("-" in last) // Handle hyphenated last names a bit better
                to_sanitize += "-";
            to_sanitize += last;
        }

        // Sanitize the raw string to make sure that it's a valid username
        var suggestion = "";
        for (int i = 0; i < to_sanitize.length; i++) {
            var c = to_sanitize[i];
            if (suggestion.length == 0) {
                if (c.isalpha()) suggestion += c.to_string();
            } else if (is_valid_username_char(c)) {
                suggestion += c.to_string();
            }
        }

        // Calculate new length that truncates and don't end with a - or _
        var newlen = int.min(suggestion.length, MAX_NAMESIZE - 1);
        while (suggestion[newlen - 1].to_string() in "-_") newlen--;

        // Return truncated string
        return suggestion.substring(0, newlen);
    }
}
